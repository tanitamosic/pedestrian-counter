#!/usr/bin/env python
# coding: utf-8

# In[65]:


import numpy as np
import cv2
from sklearn import datasets
import matplotlib
import matplotlib.pyplot as plt
from random import randint
import sys
import os

#matplotlib.rcParams['figure.figsize'] = 16,12


# In[66]:


def get_expected():
    expected = {}
    with open(os.path.join(folder, res)) as resfile:
        resfile.readline()
        line = resfile.readline()
        while line != '':
            line=line.split(", ")
            file_name = line[0]
            street_name = int(line[1][:-1])
            expected[file_name] = street_name
            line = resfile.readline()
    return expected


# In[67]:


def get_borders(path):
    cap = cv2.VideoCapture(path)
    cap.set(1, 1)
    _, frame = cap.read()
    return detect_line(frame)

def prep():
    path = "data4/video7.mp4"
    top, bottom, left, right = get_borders(path)
    # print(top, bottom, left, right)
    background = get_background(path)
    lined_bg = draw_lines(background, [top,bottom,left,right])
    # plt.imshow(lined_bg)
    # plt.show()
    return top, bottom, left, right



# In[79]:


def detect_line(img):
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    edges_img = cv2.Canny(gray_img, 100, 150, apertureSize=3)
    # plt.imshow(edges_img, "gray")
    # plt.show()
    
    min_line_length = 300
    
    lines = cv2.HoughLinesP(image=edges_img, rho=1, theta=np.pi/180, threshold=10, lines=np.array([]),
                            minLineLength=min_line_length, maxLineGap=40)
    good_lines = []
    
    for line in lines:
        
        line_coords=line[0]
        k,n = get_line_params(line_coords)
        if (50>abs(k)>10 or abs(k)<0.1) and min(line_coords[1],line_coords[3])>50: 
            good_lines.append(line[0])

    good_lines_img = draw_lines(edges_img,line)
        
    #plt.imshow(good_lines_img,"gray")
    #plt.show()
    
    for line in lines:
        line_coords=line[0]
        all_lines = draw_lines(edges_img,line)
        
    #plt.imshow(all_lines,"gray")
    #plt.show()
    
    return get_final_lines(good_lines)


# In[69]:


def get_final_lines(good_lines):
    top,bottom,left,right = [],[],[],[]
    for line in reversed(good_lines):
        #print(line)
        k,n = get_line_params(line)
        if abs(k)<5:
            if line[1]<120:
                # print("top", line)
                top = line
            elif line[1]>460:
                # print("bottom", line)
                bottom = line
        else:
            if line[0]<200:
                # print("left",line)
                left = line
            elif 550>line[0]>450:
                # print("right",line)
                right = line
    
    
    if len(left)!=0 and len(right)!=0:
        if len(top) == 0:
            top = [min(left[0],left[2]),min(left[1],left[3]),max(right[0],right[2]),min(right[1],right[3])]
        if len(bottom) == 0:
            bottom = [min(left[0],left[2]),max(left[1],left[3]),max(right[0],right[2]),max(right[1],right[3])]

        if top[0]>min(left[0],left[2]): top[0] = min(left[0],left[2])
        if top[1]>left[1]: top[1] = min(left[1],left[3])
        if top[2]>right[2]: top[2] = min(right[0],right[2])
        
   

    # print("t b l r",top,bottom,left,right)
        
    return top, bottom, left, right


# In[70]:


def get_line_params(line_coords):
    try:
        k = (float(line_coords[3]) - float(line_coords[1])) / (float(line_coords[2]) - float(line_coords[0]))
        n = k * (float(-line_coords[0])) + float(line_coords[1])
    except ZeroDivisionError:
        k = 1
        n = 0
    return k, n


# In[71]:


def detect_cross(x, y, k, n):
    yy = k*x + n
    
    return -1.5 <= (yy - y) <= 2.45


# In[72]:


def get_background(video_path):
    frame_num = 1
    cap = cv2.VideoCapture(video_path)
    
    grabbed,avg_frame = cap.read()
    avg_frame = avg_frame.astype(float)
    # width  = cap.get(3) 
    # height = cap.get(4)
    # print(width, height)
    
    while True:
        frame_num += 1
        grabbed, frame = cap.read()

        if not grabbed:
            break
        
        avg_frame += frame.astype(float)
        
    avg_frame/=frame_num
    avg_frame = avg_frame.astype("uint8")
    
    return avg_frame
    


# In[73]:


def draw_lines(img,lines):
    for line_coords in lines:
        if len(line_coords)>0:
            img = cv2.line(img,
                     (line_coords[0],line_coords[1]),
                     (line_coords[2],line_coords[3]),
                     (255,255,255),3)
    return img


# In[77]:


def process_video(video_path):
    total = 0
    k = 0
    n = 0
    
    plt.figure()
    frame_num = 0
    cap = cv2.VideoCapture(video_path)
    cap.set(1, frame_num) 
    saved_x = []
    
    background = get_background(video_path)
    
    # plt.imshow(background)
    # plt.show()
    # print(frame_num,center_x, center_y)
    while True:
        frame_num += 1
        grabbed, frame = cap.read()

        if not grabbed:
            break
        
        #if frame_num == 1:
        #     print(video_path)
        #    top,bottom,left,right = detect_line(frame)
        #    lined_bg = draw_lines(background, [top,bottom,left,right])
        #    plt.imshow(lined_bg)
        #    plt.show()
        line_coords = top

        k, n = get_line_params(line_coords)

        line_left_x = line_coords[0]
        line_right_x = line_coords[2]
        
        org_frame = frame
  
        frame = cv2.subtract(frame,background)        
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) 
        thresh, frame_bin = cv2.threshold(frame_gray, 50, 255, cv2.THRESH_BINARY)
        frame_numbers = cv2.dilate(frame_bin, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 2)), iterations=3)
        
        #if frame_num == 1: plt.imshow(frame_numbers, "gray")
        
        contours, _ = cv2.findContours(frame_numbers.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        rectangles = [cv2.boundingRect(contour) for contour in contours]
        # for rect in rectangles:
            # if rect[0] not in saved_x and rect[1]>line_coords[3]:
             #   saved_x.append(rect[0])
             #   plt.imshow(frame_numbers)
             #   plt.show()

        difs = []
        for rectangle in rectangles:
            x, y, w, h = rectangle
            if h > 1: 
                center_x = x + w / 2
                center_y = 480 - (y + h / 2)
                
                if (line_left_x <= center_x <= line_right_x):                                      
                    if (detect_cross(center_x, center_y, k, n)):
                        total+= 1
                    

    cap.release() 
    return total


# In[80]:

if __name__ == "__main__":

    if sys.argv[1] not in '-f': folder = sys.argv[1]
    else: 
        folder = "data4"
    top, bottom, left, right = prep()
    results = {}
    res = "res.txt"
    expected = get_expected()

    for file in os.listdir(folder):
        if file.endswith(".mp4"):
            results[file] = process_video(os.path.join(folder,file))
            print(file, "-", expected[file], "-", results[file])


    errors = []
    for file in results:
        # print(file, expected[file], results[file])
        errors.append(abs(expected[file]-results[file]))

    print("MAE: ", str(sum(errors)/len(errors)))






